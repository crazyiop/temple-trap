gen:
	python3 gen_problem.py

db:
	awk '{print $0}' data-normal/* > tt-normal.txt
	awk '{print $0}' data-colimacon/* > tt-colimacon.txt

cards:
	python3 cards.py
	tar -cf assets/printed_deck_normal.tar deck/normal
	tar -cf assets/printed_deck_colimacon.tar deck/colimacon
