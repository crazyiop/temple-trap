from random import choice
from PIL import Image, ImageFont, ImageDraw
import os
import qrcode
from math import ceil
from collections import defaultdict, namedtuple
import string

from game import Block

Puzzle = namedtuple(
    "Puzzle", ["blocks", "px", "py", "dist", "nb_state", "nb_win", "over"]
)


def parse(line):
    data, stepped_over = line.split("{")
    stepped_over = sorted(b.strip() for b in stepped_over.split("}")[0].split(","))
    *blocks, px, py, dist, nb_state, nb_win, _ = data.split(",")
    return Puzzle(
        blocks, int(px), int(py), int(dist), int(nb_state), int(nb_win), stepped_over
    )


def get_puzzles(db_file):
    try:
        with open(f"tt-{db_file}.txt", "r") as fp:
            lines = fp.readlines()
    except FileNotFoundError:
        raise FileNotFoundError(
            "run `python3 gen_problem.py` (very time comsuming) and `cat data/* > tt.txt` first"
        )

    print(f"{len(lines)} puzzles in txt file")

    puzzles = [parse(line) for line in lines]

    # same dist, stepped_over
    s = defaultdict(list)
    for p in puzzles:
        over = "".join(sorted(p.over))
        s[p.dist, over].append(p)
    keep = []
    for k in s.keys():
        keep.append(choice(s[k]))
    print(f"{len(keep)} puzzle kept after (dist, steped-over) pruning")

    # same dist, same blocks
    s = defaultdict(list)
    for p in keep:
        b = "".join(sorted(p.blocks))
        s[p.dist, b].append(p)
    keep = []
    for k in s.keys():
        keep.append(choice(s[k]))
    print(f"{len(keep)} puzzle kept after (dist, blocks) pruning")

    # same dist, same nb_state
    s = defaultdict(list)
    for p in keep:
        s[p.dist, p.nb_state].append(p)
    keep = []
    for k in sorted(s.keys(), reverse=True):
        keep.append(choice(s[k]))
    print(f"{len(keep)} puzzle kept after (dist, nb_state) pruning")

    # same nb_state
    s = defaultdict(list)
    for p in keep:
        s[p.nb_state].append(p)
    keep = []
    for k in sorted(s.keys(), reverse=True):
        keep.append(max(s[k], key=lambda p: p.dist))
    print(f"{len(keep)} puzzle kept after (nb_state) pruning")

    keep.sort(key=lambda p: p.dist)
    return keep


Coord = namedtuple("Coord", ["x", "y"])
Size = namedtuple("Size", ["x", "y"])

# Cards parameter
BLEED = Size(747, 1122)
SAFE = Size(615, 990)
CUT = Size(650, 1050)
safe = Coord((BLEED.x - SAFE.x) // 2, (BLEED.y - SAFE.y) // 2)
cut = Coord((BLEED.x - CUT.x) // 2, (BLEED.y - CUT.y) // 2)

# Puzzle drawing parameter
safe_margin = 55  # content to safe
content_size = Size(SAFE.x - 2 * safe_margin, SAFE.x - 2 * safe_margin)
header_size = Size(
    SAFE.x - 2 * safe_margin, (SAFE.y - content_size.y - 4 * safe_margin) // 2
)

header_ori = Coord(safe.x + safe_margin, safe.y + safe_margin)
content_ori = Coord(safe.x + safe_margin, safe.y + 2 * safe_margin + header_size.y)
footer_ori = Coord(
    safe.x + safe_margin, safe.y + 3 * safe_margin + header_size.y + content_size.y
)

colors = {
    "border": "BlanchedAlmond",
    "up": "limegreen",
    "down": "burlywood",
    "sea": "deepskyblue",
    "pawn": "greenyellow",
}


def _img_merge(bg_img, fg_img, coord=(0, 0)):
    # inspire by https://stackoverflow.com/a/53663233
    # merge top left corner of fg_img at coord
    fg_img_trans = Image.new("RGBA", fg_img.size)
    fg_img_trans = Image.blend(fg_img_trans, fg_img, 1)
    bg_img.paste(fg_img_trans, coord, fg_img_trans)


def _add_debug_outline(img):
    draw = ImageDraw.Draw(img)
    draw.rectangle([cut.x, cut.y, cut.x + CUT.x, cut.y + CUT.y], outline="black")
    draw.rectangle([safe.x, safe.y, safe.x + SAFE.x, safe.y + SAFE.y], outline="black")


def mk_card(safe_content):
    card = Image.new("RGBA", BLEED)
    _img_merge(card, safe_content, (safe_margin, safe_margin))
    return card


def mk_puzzle_card(puzzle, n, debug=False, deck_id=None):
    header = mk_header(puzzle, n=n, debug=debug)
    content = mk_content(puzzle, debug=debug)
    footer = mk_footer(puzzle, debug=debug)

    card = Image.new("RGBA", BLEED)
    _img_merge(card, header, header_ori)
    _img_merge(card, content, content_ori)
    _img_merge(card, footer, footer_ori)

    # Add a deck id
    if deck_id is None:
        deck_id = " "

    font = ImageFont.truetype("assets/anquietas.ttf", 40)
    draw = ImageDraw.Draw(card)
    tx, ty = draw.textsize(deck_id, font=font)
    draw.text(
        (BLEED.x - safe.x - tx * 1.5, BLEED.y - safe.y - ty * 1.5),
        deck_id,
        (100, 100, 100),
        font=font,
    )

    if debug:
        _add_debug_outline(card)
    return card


def mk_solution_card(end_state, solution, debug=False, deck_id=None):
    content = mk_content(end_state, size=Size(197, 197), debug=debug)

    card = Image.new("RGBA", BLEED)
    _img_merge(card, content, ((BLEED.x - 197) // 2, header_ori.y))

    font = ImageFont.truetype("assets/Arrows.ttf", 40)
    draw = ImageDraw.Draw(card)
    ox, oy = content_ori.x, header_ori.y + 197 + 20
    n = 12
    delta = content_size.x // n

    for i, c in enumerate(solution):
        y, x = divmod(i, n)
        color = (0, 0, 0) if c.islower() else (50, 150, 50)
        c = {"u": "C", "d": "D", "l": "B", "r": "A"}[c.lower()]

        draw.text((ox + x * delta, oy + y * delta), c, color, font=font)

    if debug:
        _add_debug_outline(card)
    return card


def mk_info_card(puzzle, n, end_state, solution, debug=False, deck_id=None):
    card = Image.new("RGBA", Size(BLEED.x * 2, BLEED.y))
    offset = BLEED.x

    # right side, as usual
    right = mk_puzzle_card(puzzle, n, debug, deck_id)
    _img_merge(card, right, (0, 0))

    # left side, solution and end state
    left = mk_solution_card(end_state, solution, debug, deck_id)
    _img_merge(card, left, (BLEED.x, 0))

    if debug:
        _add_debug_outline(card)
    return card


def _find_font_size(size):
    # find the max font size to write the number tick given the box size
    draw = ImageDraw.Draw(Image.new("RGBA", size))
    fontsize = 100
    ty = 999_999
    while ty * 1.5 > size.y or ty * 3.5 > size.x:
        fontsize -= 1
        font = ImageFont.truetype("assets/Graduate-Regular.ttf", fontsize)
        tx, ty = draw.textsize("M", font=font)
    width = int(ty * 1.5) + 2 * tx
    ori = Coord((size.x - width) // 2, (size.y - ty) // 2)
    return fontsize, ori


def mk_number_tick(n, size):
    number = Image.new("RGBA", size)
    draw = ImageDraw.Draw(number)

    font_size, ori = _find_font_size(size)
    font = ImageFont.truetype("assets/Graduate-Regular.ttf", font_size)
    tx, ty = draw.textsize("M", font=font)

    # Center the thing
    draw.rectangle([ori.x, ori.y, ori.x + ty, ori.y + ty], None, (0, 0, 0))
    draw.text(
        [ori.x + int(ty * 1.5), ori.y, ori.x + int(ty * 2.5), ori.y + ty],
        f"{n:02}",
        (0, 0, 0),
        font=font,
    )
    return number


def mk_tick_card(id_list, bg=None, col=3):
    r = Image.new("RGBA", BLEED)
    v = Image.new("RGBA", BLEED)

    # we need to fit n+1 cell (for name)
    n = len(id_list)
    nb_rows = ceil((n + 1) / col)
    rows_recto = nb_rows // 2
    rows_verso = nb_rows - rows_recto

    st = Size(SAFE.x // 3, SAFE.y // rows_recto)

    draw = ImageDraw.Draw(r)
    font_size, ori = _find_font_size(st)
    font = ImageFont.truetype("assets/Graduate-Regular.ttf", font_size)

    # recto
    for y in range(rows_recto):
        for x in range(col):
            i = y + x * rows_recto
            if i == 0:
                draw.text([safe.x + 10, safe.y + ori.y], ">", (0, 0, 0), font=font)
                pass
            else:
                number = mk_number_tick(i, st)
                _img_merge(r, number, (safe.x + x * st.x, safe.y + y * st.y))
    start = i + 1

    # recto
    for y in range(rows_verso):
        for x in range(col):
            i = y + x * rows_verso + start
            if i <= n:
                number = mk_number_tick(i, st)
                _img_merge(v, number, (safe.x + x * st.x, safe.y + y * st.y))

    if bg is not None:
        r = Image.alpha_composite(bg, r)
        v = Image.alpha_composite(bg, v)
    return r, v


def mk_credits():
    # content only first
    card = Image.new("RGBA", SAFE)
    draw = ImageDraw.Draw(card)

    base_font_size = 25
    currentx, currenty = 15, 15
    txt="""
    ##Temple Trap unofficial big set
    For more informations and pdf version of this deck go at:
    harder-puzzles.silicub.fr

    ##Printed deck
    Can be ordered at:
    www.printerstudio.com/sell/harder-puzzles

    #Made by
    Jonathan Nifenecker, 2020
    """

    for line in txt.split('\n'):
        text = line.strip()
        if not text:
            currenty += base_font_size // 2
        else:
            fontsize = base_font_size + [0, 6, 8][text.count("#")]
            indent = base_font_size * [2, 0, 0][text.count("#")]
            text = text.strip("#")
            font = ImageFont.truetype("assets/PT_Sans-Narrow-Web-Regular.ttf", fontsize)
            tx, ty = draw.textsize(text, font=font)
            draw.text((currentx + indent, currenty), text, "black", font=font)
            currenty += ty + ty // 4  # 125% interline

    qr = qrcode.QRCode(box_size=7)
    qr.add_data("harder-puzzles.silicub.fr")
    qr.make(fit=True)
    qr_img = qr.make_image().convert("RGBA")

    qrx = (SAFE.x - qr_img.size[0]) // 2
    qry = currenty + (SAFE.y - currenty - qr_img.size[1]) // 2
    _img_merge(card, qr_img, (qrx, qry))

    full_card = Image.new("RGBA", BLEED)
    _img_merge(full_card, card, safe)
    return full_card


# Game specific content filler
##############################
def mk_front(db_file, size=BLEED, debug=False):
    card = Image.new("RGBA", size)
    draw = ImageDraw.Draw(card)
    unit = 14 * 6
    X = size.x // unit + 1
    Y = size.y // unit + 1
    fixed = {
        (2, 5): "r",
        (3, 5): "lrd",
        (4, 5): "l",
        (3, 6): "ud",
        (3, 7): "u",
        (4, 6): "R",
        (5, 6): "LRD",
        (6, 6): "L",
        (5, 7): "UD",
        (5, 8): "U",
        (4, 8): "urld",
    }
    for x in range(X):
        for y in range(Y):
            if db_file == "normal":
                b = choice(
                    ["ur", "dr", "lu", "ld"] * 3
                    + ["UR", "DR", "LU", "LD"] * 2
                    + ["dU", "lR", "uD", "rL"] * 2
                    + ["UD", "LR"] * 2
                    + ["X"] * 3
                )
            else:
                b = choice(
                    ["ur", "dr", "lu", "ld"] * 3
                    + ["UR", "DR", "LU", "LD"] * 2
                    + ["rU", "uL", "lD", "dR"]
                    + ["lU", "dL", "rD", "uR"]
                    + ["UD", "LR"] * 2
                    + ["X"] * 3
                )
            if (x, y) in fixed:
                block = mk_block(fixed[x, y], Size(unit, unit), (x, y) == (4, 8))
            else:
                block = mk_block(b, Size(unit, unit))
            _img_merge(card, block, (unit * x, unit * y))
    for x in range(X):
        draw.line([x * unit, 0, x * unit, size.y], fill="black")
    for y in range(Y):
        draw.line([0, y * unit, size.x, y * unit], fill="black")

    if debug:
        # Add tuck box window visualization
        mr = 189
        mt = 336
        mb = 177
        draw.rectangle([mr, mt, size.x - mr, size.y - mb], outline="red")
    return card


def mk_header(puzzle, size=header_size, n=132, debug=False):
    header = Image.new("RGBA", size)
    draw = ImageDraw.Draw(header)
    if debug:
        draw.rectangle([0, 0, size.x, size.y], fill="blue")

    font = ImageFont.truetype("assets/Graduate-Regular.ttf", 100)
    nx, ny = draw.textsize(str(n), font=font)
    draw.text((0, 0), str(n), (0, 0, 0), font=font)

    font = ImageFont.truetype("assets/Graduate-Regular.ttf", 50)
    text = f" {puzzle.dist}"
    tx, ty = draw.textsize(text, font=font)
    draw.text((nx, ny - ty), text, (100, 100, 100), font=font)
    return header


def mk_block(b, size, pawn=False):
    block = Image.new("RGBA", size)
    unit = size.x // 4
    psize = int(size.x / 2.7)
    draw = ImageDraw.Draw(block)

    if b == "X":
        draw.rectangle([0, 0, size.x, size.y], fill=colors["sea"])
        return block

    if any(c.islower() for c in b):
        bg = colors["down"]
    else:
        bg = colors["up"]
    draw.rectangle([0, 0, size.x, size.y], fill=bg)

    if any(c.islower() for c in b) and any(c.isupper() for c in b):
        step_width = size.x // 13
        gap = step_width // 2
        if "U" in b:
            start = [0, 0, size.x, step_width]
            delta = [0, step_width + gap, 0, step_width + gap]
        if "L" in b:
            start = [0, 0, step_width, size.y]
            delta = [step_width + gap, 0, step_width + gap, 0]
        if "D" in b:
            start = [0, size.y - step_width - 1, size.x, size.y]
            delta = [0, -step_width - gap - 1, 0, -step_width - gap - 1]
        if "R" in b:
            start = [size.x - step_width - 1, 0, size.x, size.y]
            delta = [-step_width - gap - 1, 0, -step_width - gap - 1, 0]

        for _ in range(3):
            draw.rectangle(start, fill=colors["up"])
            start = [s + d for s, d in zip(start, delta)]

    if "u" not in b.lower():
        draw.rectangle([0, 0, size.x, unit], fill=colors["border"])
    if "l" not in b.lower():
        draw.rectangle([0, 0, unit, size.y], fill=colors["border"])
    if "d" not in b.lower():
        draw.rectangle([0, size.y - unit, size.x, size.y], fill=colors["border"])
    if "r" not in b.lower():
        draw.rectangle([size.x - unit, 0, size.x, size.y], fill=colors["border"])

    if pawn:
        draw.ellipse(
            [psize, psize, size.x - psize, size.y - psize],
            fill=colors["pawn"],
            outline="grey",
        )
    return block


def mk_content(puzzle, size=content_size, debug=False):
    content = Image.new("RGBA", size)
    draw = ImageDraw.Draw(content)
    if debug:
        draw.rectangle([0, 0, size.x, size.y], fill="grey")

    unit = size.x // 14

    draw.rectangle(
        [0, 0, size.x - 1, size.y - 1], fill=colors["border"], outline="black"
    )

    draw.rectangle([0, 2 * unit + 1, unit, 4 * unit - 1], fill=colors["up"])
    draw.line([0, 2 * unit, 0, 4 * unit], fill="black")

    for i, b in enumerate(puzzle.blocks):
        y, x = divmod(i, 3)
        block = mk_block(b, Size(unit * 4, unit * 4), (x, y) == (puzzle.px, puzzle.py))
        _img_merge(content, block, coord=((1 + 4 * x) * unit, (1 + 4 * y) * unit))
    for i in range(4):
        draw.line(
            [unit, unit + i * unit * 4, size.x - unit - 1, unit + i * unit * 4],
            fill="black",
        )
        draw.line(
            [unit + i * unit * 4, size.y - unit - 1, unit + i * unit * 4, unit],
            fill="black",
        )
    return content


def mk_footer(puzzle, size=header_size, debug=False):
    footer = Image.new("RGBA", size)
    draw = ImageDraw.Draw(footer)
    if debug:
        draw.rectangle([0, 0, size.x, size.y], fill="red")

    font = ImageFont.truetype("assets/Graduate-Regular.ttf", 40)
    puzzle_id = (
        "#"
        + "".join(Block(b).to_id() for b in puzzle.blocks)
        + f"{puzzle.px+3*puzzle.py}"
    )

    tx, ty = draw.textsize(puzzle_id, font=font)
    draw.text(
        ((size.x - tx) // 2, (size.y - ty) // 2), puzzle_id, (100, 100, 100), font=font
    )

    return footer


def gen_deck(n, db_file, offset=0, deck_id=None):
    print(f"generating {db_file} deck")
    try:
        os.makedirs(os.path.join("deck", db_file, "recto"))
        os.makedirs(os.path.join("deck", db_file, "verso"))
    except FileExistsError:
        pass

    front = mk_front(db_file)
    front.save(os.path.join("deck", db_file, "recto", "00.png"), "PNG")
    credit = mk_credits()
    credit.save(os.path.join("deck", db_file, "verso", "00.png"), "PNG")

    keep = get_puzzles(db_file)

    if offset:
        keep = keep[:-offset]
    selected = keep[-n:]

    if deck_id is None:
        deck_id = "".join(choice(string.ascii_lowercase) for _ in range(3))

    for i, puzzle in enumerate(selected, start=1):
        card = mk_puzzle_card(puzzle, i, debug=False, deck_id=deck_id)
        if i % 2:
            folder = "recto"
            n = (i + 1) // 2
        else:
            folder = "verso"
            n = (i + 1) // 2
        card.save(
            os.path.join("deck", db_file, folder, f"{n:02}{folder[0]}.png"), "PNG"
        )
        print(i)

    front.putalpha(256 // 4)
    tr, tv = mk_tick_card(list(range(1, 101)), front)
    tr.save(os.path.join("deck", db_file, "recto", "99.png"), "PNG")
    tv.save(os.path.join("deck", db_file, "verso", "99.png"), "PNG")


if __name__ == "__main__":
    gen_deck(100, "colimacon")
    gen_deck(100, "normal")
