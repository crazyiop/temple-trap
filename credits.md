# Solving temple-trap

## Code
All the source code that allow to :
 - generate the puzzles database and to prune it (very cpu intensive!!)
 - generate the graphics for the card game deck
 - find a minimal solution from any given situation

are available at: https://gitlab.com/crazyiop/temple-trap

## Printed deck
Cards printed by the www.printerstudio.com service

## Credits: fonts
 - [Graduate](https://fonts.google.com/specimen/Graduate) for cards numbers.
 - [PT Sans Narrow](https://fonts.google.com/specimen/PT+Sans+Narrow) for credits.
 - [Anquietas](https://fonts2u.com/anquietas.font) from Joseph Spicer for deck\_id.
 - [Arrows](https://www.dafont.com/fr/arrows.font) from FontLab Studio 5 for the solution's arrows.
