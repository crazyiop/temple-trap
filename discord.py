import os
from fpdf import FPDF as PDF


from game import Block, Game, solve
from cards import *


def get_same_puzzles(db_file, filename):
    """
    try:
        with open(f"{filename}.txt", "r") as fp:
            print(f"reusing existing {filename}.txt file")
            lines = fp.readlines()
        puzzles = [parse(line) for line in lines]
        return puzzles
    except FileNotFoundError:
        # Generate this file if not present
        pass

    with open(f"{filename}{db_file}.txt", "w") as fp:
        for p in keep:
            fp.write(",".join(b for b in p.blocks))
            fp.write(f",{p.px},{p.py},{p.dist},{p.nb_state},{p.nb_win},")
            over = p.over
            fp.write("{")
            fp.write(", ".join(b for b in p.over))
            fp.write("}\n")
    """
    keep = get_puzzles(db_file)

    return keep


def gen_deck(db_file):
    print(f"generating {db_file} deck")
    path = os.path.join("discord", "image")
    try:
        os.makedirs(path)
    except FileExistsError:
        pass

    front = mk_front(db_file)
    front.save(os.path.join(path, "000.png"), "PNG")

    keep = get_same_puzzles(db_file, "discord")[-200:]

    for i, puzzle in enumerate(keep, start=1):
        card = mk_puzzle_card(puzzle, i, debug=False)
        card.save(os.path.join(path, f"{i:03}.png"), "PNG")
        print(i)  # , puzzle.dist)

    """
    tr, tv = mk_tick_card(list(range(1, 101)), front)
    tr.save(os.path.join(path, "998_score_card_recto.png"), "PNG")
    tr.save(os.path.join(path, "999_score_card_verso.png"), "PNG")
    """


def make_pdfs():
    X, Y = 2.49, 3.74

    big = PDF(unit="in")
    ox, oy = (8.3 - 3 * X) / 2, (11.7 - 3 * Y) / 2

    small = PDF(unit="in", format=(X, Y))

    for pdf in [big, small]:
        pdf.set_title("Temple Trap unofficial extension set")
        pdf.set_author("crazyiop@gitlab")

    it = iter(sorted(os.listdir(os.path.join("discord", "image")))[-202:])
    front = next(it)  # Cover will be reworked with libreoffice...
    small.add_page()
    big.add_page()
    small.image(os.path.join("discord", "image", front), w=2.49, x=0, y=0)
    big.image(os.path.join("discord", "image", front), w=2.49, x=ox+X, y=oy+Y)
    small.add_page()

    try:
        while True:
            big.add_page()
            for y in range(3):
                for x in range(3):
                    image = next(it)
                    print(image)
                    big.image(
                        os.path.join("discord", "image", image),
                        w=2.49,
                        x=ox + x * 2.49,
                        y=oy + y * 3.74,
                    )
                    small.add_page()
                    small.image(
                        os.path.join("discord", "image", image), w=2.49, x=0, y=0
                    )
    except StopIteration:
        pass
    big.output("big.pdf")
    small.output("small.pdf")


if __name__ == "__main__":
    gen_deck("colimacon")
    make_pdfs()
