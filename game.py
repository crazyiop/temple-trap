from copy import copy
from collections import deque
import argparse
import os
from termcolor import colored

red = lambda s: colored(s, 'red')

def rotate(nible):
    c = nible >> 3
    new = ((nible << 1) & 0x0F) + c
    return new


class Block:
    """
    Instance of a block, one of the 8 'maze' part of the game
    Uppercase letter are upper level, lowercase letter lower level:
    ('Ud' for example is the stairs, upper side up, lower side down)
    """

    bit = {"u": 8, "r": 4, "d": 2, "l": 1, "x": 0}
    shift = {True: 0, False: 4}

    def __init__(self, sides=None):
        # Side is stored as follow in bit: urdl (level1) - urdl (level2)
        self.sides = 0
        if isinstance(sides, str):
            for c in sides:
                self.sides += Block.bit[c.lower()] << Block.shift[c.isupper()]
        elif isinstance(sides, int):
            self.sides = sides

    def __str__(self):
        to_char = {
            10: "║",
            5: "═",
            12: "╚",
            6: "╔",
            3: "╗",
            9: "╝",
            192: "└",
            96: "┌",
            48: "┐",
            144: "┘",
            130: "▼",
            65: "◄",
            40: "▲",
            20: "►",
            0: "X",
            72: "╙",
            129: "╛",
            18: "╖",
            36: "╒",
            24: "╜",
            33: "╕",
            66: "╓",
            132: "╘",
        }
        try:
            return to_char[self.sides]
        except KeyError:
            print(f"no str for {self.sides}")
            return "?"

    def __repr__(self):
        to_str = {
            10: "UD",
            5: "LR",
            12: "UR",
            6: "DR",
            3: "LD",
            9: "LU",
            192: "ur",
            96: "dr",
            48: "ld",
            144: "lu",
            130: "uD",
            65: "rL",
            40: "dU",
            20: "lR",
            0: "X",
            72: "rU",
            129: "uL",
            18: "lD",
            36: "dR",
            24: "lU",
            33: "dL",
            66: "rD",
            132: "uR",
        }
        return to_str[self.sides]

    def __hash__(self):
        return hash(self.sides)

    def __lt__(self, other):
        return self.sides < other.sides

    def __eq__(self, other):
        return self.sides == other.sides

    @property
    def up(self):
        return (self.sides >> 3) & 0x11

    @property
    def right(self):
        return (self.sides >> 2) & 0x11

    @property
    def down(self):
        return (self.sides >> 1) & 0x11

    @property
    def left(self):
        return (self.sides >> 0) & 0x11

    def canonize(self):
        return min(self.rotations(), key=lambda B: B.sides).sides

    def rotations(self):
        res = set()
        nl = self.sides & 0x0F
        nh = self.sides >> 4
        for _ in range(4):
            nh = rotate(nh)
            nl = rotate(nl)
            newsides = (nh << 4) + nl
            res.add(Block(newsides))
        return list(res)

    def to_id(self):
        d = [
            "",
            "ur",
            "dr",
            "lu",
            "ld",
            "UR",
            "DR",
            "LU",
            "LD",
            "dU",
            "lR",
            "uD",
            "rL",
            "UD",
            "LR",
            "X",
            "uR",
            "lU",
            "uL",
            "dL",
            "dR",
            "rU",
            "lD",
            "rD",
        ]
        return chr(ord("0") + d.index(repr(self)))


class Game:
    """
    Represent a state of the game
    """

    def __init__(self, places, pawn, no_block=None):
        if isinstance(places, dict):
            self.places = places
        else:
            self.places = {}
            for y in range(3):
                for x in range(3):
                    self.places[x, y] = places.pop(0)
        self.pawn = pawn

        if no_block is None:
            # no_block serve as a quick and easy reference to the empty block,
            # it will allow to skip a lot of scanning to get it
            for k, v in self.places.items():
                if v.sides == 0:
                    self.no_block = k
                    break
        else:
            self.no_block = no_block

        self.s = ""
        for y in range(3):
            for x in range(3):
                c = str(self.places[x, y])
                if self.pawn == (x, y):
                    self.s += red(c)
                else:
                    self.s += c
            self.s += "\n"

    def __str__(self):
        return self.s

    @classmethod
    def from_str(cls, s):
        *cells, px, py = s.split(",")
        places = {}
        for y in range(3):
            for x in range(3):
                places[x, y] = Block(cells.pop(0))
        return Game(places, (int(px), int(py)))

    def __repr__(self):
        blocks = []
        for y in range(3):
            for x in range(3):
                blocks.append(repr(self.places[x, y]))
        x, y = self.pawn
        return ",".join(blocks) + f",{x},{y}"

    def __eq__(self, other):
        return str(self) == str(other)

    def __hash__(self):
        return hash(str(self))

    def walkable(self):
        """
        Gives the possible movement of the pawn, current position included
        """
        # build place pawn can go to
        if self.pawn == (-1, 0):
            if self.places[0, 0].left & 0x0F:
                return [(0, 0)]
            return []

        walked = []
        x, y = self.pawn
        # up
        if y != 0:
            if self.places[self.pawn].up & self.places[x, y - 1].down:
                walked.append((x, y - 1))
        # right
        if x != 2:
            if self.places[self.pawn].right & self.places[x + 1, y].left:
                walked.append((x + 1, y))
        # down
        if y != 2:
            if self.places[self.pawn].down & self.places[x, y + 1].up:
                walked.append((x, y + 1))
        # left
        if x != 0:
            if self.places[self.pawn].left & self.places[x - 1, y].right:
                walked.append((x - 1, y))

        # outside = win
        if self.pawn == (0, 0) and self.places[0, 0].left & 0x0F:
            walked.append((-1, 0))

        return walked

    def _moves_pawn(self):
        moves = []
        for pos in self.walkable():
            moves.append(type(self)(self.places, pos, self.no_block))
        return moves

    def _move_block(self, delta):
        px, py = self.no_block
        dx, dy = delta
        nx = px + dx
        ny = py + dy
        if 0 <= nx < 3 and 0 <= ny < 3 and (nx, ny) != self.pawn:
            places = copy(self.places)
            places[px, py] = places[nx, ny]
            places[nx, ny] = Block()
            return type(self)(places, self.pawn, (nx, ny))
        return None

    def _moves_block(self):
        """
        Gives the possible next state when moving a block
        """
        moves = []
        if self.pawn == (-1, 0) or (self.places[self.pawn].sides & 0xF0):
            # can move block only if on lower level or when 'outside'
            for delta in [(0, -1), (1, 0), (0, 1), (-1, 0)]:
                move = self._move_block(delta)
                if move:
                    moves.append(move)
        return moves

    def moves(self):
        """
        Return a list of possible next state, moving the pawn or a block
        """
        return self._moves_pawn() + self._moves_block()

    def is_possible(self):
        """
        does not garantee possibility but exclude obviously impossible game
        """
        s = set(self.places.values())
        if (
            Block("Lr") in s
            or (Block("LD") in s and Block("Ud") in s)
            or (
                Block("LR") in s
                and Block("LD") in s
                and Block("UL") in s
                and Block("Rl") in s
            )
            or set(Block("Ur").rotations()) & s
            or set(Block("Ul").rotations()) & s
        ):
            return True
        return False

    def __sub__(self, other):
        """
        return movement done to go from other to self. Upper letter: pawn, Lower: block
        """
        sx, sy = self.pawn
        ox, oy = other.pawn
        if sx - ox == 1:
            return "R"
        if sx - ox == -1:
            return "L"
        if sy - oy == 1:
            return "D"
        if sy - oy == -1:
            return "U"

        sx, sy = self.no_block
        ox, oy = other.no_block
        if sx - ox == 1:
            return "l"
        if sx - ox == -1:
            return "r"
        if sy - oy == 1:
            return "u"
        if sy - oy == -1:
            return "d"


def graph(state, verbose=True):
    """
    Visit from the given state every possible state and return :
    - a listlist graph format (int indexes)
    - a dictionary that makes a correspondance from the above int, and the game state
    - a list of win states (int)
    - the int corresponding to the given state parameter
    """

    n_to_game = {}
    game_to_n = {}
    G = []
    heap = deque([state])
    win = []
    n = 0
    turn = 0
    if verbose:
        print("visiting the whole graph: ", end="")
    while heap:
        if verbose:
            turn += 1
            if turn % 10000 == 0:
                print(".", end="", flush=True)
        node = heap.popleft()
        if node in game_to_n:
            continue
        game_to_n[node] = n
        n_to_game[n] = node

        if node.pawn == (-1, 0):
            win.append(n)
        moves = node.moves()
        G.append(moves)
        heap += moves
        n += 1

    if verbose:
        print()
        print("creating listlist graph format...")

    g = []
    for l in G:
        g.append([game_to_n[move] for move in l])
    if verbose:
        print(f"{len(g)} states, {len(win)} win positions")
    return g, n_to_game, win, game_to_n


def bfs(graph, starts):
    """
    return distance and precedence node from a graph from multiple start position
    """
    to_visit = deque()
    dist = [float("inf")] * len(graph)
    prec = [None] * len(graph)
    for start in starts:
        dist[start] = 0
        to_visit.appendleft(start)
    while to_visit:
        node = to_visit.pop()
        for neighbor in graph[node]:
            if dist[neighbor] == float("inf"):
                dist[neighbor] = dist[node] + 1
                prec[neighbor] = node
                to_visit.appendleft(neighbor)

    return dist, prec


def solve(game, verbose=True):
    """
    From a game state, give the minimum step list to a solved state
    """
    if verbose:
        print(game)
    G, n_to_game, win, game_to_n = graph(game, verbose=verbose)
    n_game = game_to_n[game]
    if verbose and not win:
        print("impossible game")
        return []

    dist, prec = bfs(G, starts=win)
    if verbose:
        print(f"solution in {dist[n_game]} steps:")
    step = 0

    steps = []

    while prec[n_game] is not None:
        step += 1
        steps.append(n_to_game[n_game])
        n_game = prec[n_game]

    return G, win, steps


def stepped_over(steps):
    """
    for a given steps list, gives the blocklist of the block who are stepped
    over during the solve
    /!\\ This is a naive weak implementation: if multiple similar blocks are in the same orientation
    they will be counted as one (losing the difference between only one and several being used)...
    After testing, the loss of potentials game seems not that big and we still have more than
    enough left, so this will be considered 'close enough'.
    """
    blocks = set()
    for step in steps:
        blocks.add(step.places[step.pawn])
    return blocks


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "gamestring",
        type=str,
        metavar="gamestring",
        help="game in the 'LD,X,Ud,UD,ur,ld,UR,Lr,lu,2,0' format",
    )
    # parser.add_argument("--graph", help="produce a graph visualization", action="store_true")
    args = parser.parse_args()
    game = Game.from_str(args.gamestring)

    G, win, steps = solve(game)

    _, columns = map(int, os.popen("stty size", "r").read().split())
    i = 0
    n = (columns - 2) // 5

    while steps:
        to_print = steps[:n]
        steps = steps[n:]
        print("  ".join(f"{c:3d}" for c in range(i, i + len(to_print))))
        for l in range(3):
            print("  ".join(str(step).split("\n")[l] for step in to_print))
        i += n
        print()
