from collections import Counter
from itertools import product, combinations_with_replacement, chain
import multiprocessing
import os
from random import randint

from game import Block, Game, graph, bfs, stepped_over


def analyse(game):
    G, n_to_game, win, game_to_n = graph(game, verbose=False)
    dist, prec = bfs(G, starts=win)

    while True:
        # Ensure to give a worst place by only considering the state where the pawn is donwstairs
        most_mvt = max(dist)
        worst_start_n = dist.index(most_mvt)
        worst_start = n_to_game[worst_start_n]
        try:
            if worst_start.places[worst_start.pawn].sides & 0xF0:
                break
        except KeyError:
            return None, 0, 0, 0, set()
        dist[worst_start_n] = -1

    steps = []
    while prec[worst_start_n] is not None:
        steps.append(n_to_game[worst_start_n])
        worst_start_n = prec[worst_start_n]
    over = stepped_over(steps)
    return repr(worst_start), most_mvt, len(G), len(win), over


def gen_chunks(gameset):
    name, blocks_set = gameset

    combs = [
        combinations_with_replacement(Block(b).rotations(), n) for b, n in blocks_set
    ]

    for ts in product(*combs):
        blocks = sorted(chain(*ts, [Block("X")]))
        yield (name, blocks[::])
        c = Counter(blocks)
        if 2 not in c.values() and 3 not in c.values():
            yield (name, blocks[:-2] + [blocks[-1], blocks[-2]])


def treat_chunk(chunk):
    n = randint(1, 100)
    name, blocks = chunk
    filename = os.path.join(f"data-{name}", "".join(repr(s) for s in blocks) + ".txt")
    print(f">{n}\tstarting {filename}")
    if not os.path.exists(filename):
        game = Game(blocks, (-1, 0))
        if game.is_possible():
            position, dist, l_graph, l_win, over = analyse(game)
            if l_graph:
                with open(filename, "w") as fp:
                    fp.write(f"{position},{dist},{l_graph},{l_win},{over}")
    print(f"<{n}\tended {filename}")


def pool_treatment(gameset):
    """
    for c in gen_chunks(gameset):
        treat_chunk(c)
    """
    cpu = multiprocessing.cpu_count()
    if cpu <= 8:
        # Stay easy on a peronal computer
        cpu -= 1
    it = gen_chunks(gameset)
    _, blocks = next(it)
    while set(blocks) != {
        Block(b) for b in ["X", "LD", "LR", "UR", "dR", "ld", "rD", "dr", "ur"]
    }:
        _, blocks = next(it)

    pool = multiprocessing.Pool(cpu)
    pool.map_async(treat_chunk, gen_chunks(gameset))
    pool.close()
    pool.join()
    print("done")


if __name__ == "__main__":

    gamesets = [
        ("colimacon", [("lu", 3), ("LU", 2), ("Lu", 1), ("Ld", 1), ("LR", 1)]),
        ("normal", [("lu", 3), ("LU", 2), ("lR", 2), ("LR", 1)]),
    ]

    for gameset in gamesets:
        name, _ = gameset
        path = f"data-{name}"
        try:
            os.makedirs(path)
        except FileExistsError:
            pass

        pool_treatment(gameset)
