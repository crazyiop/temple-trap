from PIL import Image, ImageFont, ImageDraw
from cards import get_puzzles, mk_content, Puzzle, Size
from game import Game, solve
from random import shuffle


def mk_gif(steps, filename="test.gif", delay=400):
    if isinstance(steps[0], Puzzle):
        puzzles = steps
    elif isinstance(steps[0], Game):
        puzzles = []
        for step in steps:
            blocks = []
            for y in range(3):
                for x in range(3):
                    blocks.append(repr(step.places[x, y]))
            p = Puzzle(blocks, step.pawn[0], step.pawn[1], 0, 0, 0, "")
            puzzles.append(p)
    else:
        raise ValueError(f"cannot use {type(steps[0])} for the gif creation")

    frames = [mk_content(p, size=Size(210, 210)) for p in puzzles]
    frames[0].save(
        filename,
        save_all=True,
        append_images=frames[1:],
        optimize=False,
        duration=delay,
        loop=0,
    )


if __name__ == "__main__":
    for gameset in ["normal", "colimacon"]:
        keep = get_puzzles(gameset)
        shuffle(keep)
        mk_gif(keep[:20], f"assets/{gameset}.gif", delay=300)

    game = Game.from_str("LD,X,Ud,UD,ur,ld,UR,Lr,lu,2,0")
    G, win, steps = solve(game)
    mk_gif(steps, "assets/solution.gif")
