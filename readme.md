# Quick PDF Link

If you land on this page and are just looking for the small pdf files to play more puzzle, this is it:
 - [200 hard puzzles with the base game](normal-small.pdf)
 - [200 hard puzzles with the DIY colimacon stairs](colimacon-small.pdf)

# Solving Temple-trap

This projec aim to find all interresting puzzle of the board game ['temple trap'](https://www.smartgames.eu/uk/one-player-games/temple-trap) from Smart Games.

My aim is to create a set of complex puzzles to continue playing after finishing the provided booklet.

## Step 1: Creating the database

### game.py
This file allow to manipulate a game state and create a graph of the position that can be transition from each other. This allow me to search for a minimal solution for any game:

![1](assets/temple-solution.png)

![1](assets/solution.gif)

### gen\_problem.py
This file scan the full space exploration of all possible gamestate an a parallelized way. This is taking a lot of computation as every valid set can take between 10 to 30s to compute (on my pc). The way I scan the possibles set of pieces give me this only constraint:

 - Only one puzzle is made from a unique placing and orientations of the 8 blocks: The one who is the farthest away in number of move from a solved state.

This is a limit I'm ok with. I still has plenty of puzzle generated, and this choice greatly improve the efficiency I can have in scanning the whole space.

Crunching the number was done on an AWS 72 core computation instance. It took ~6-8h to completely scan:

 - the normal set of block of the game.
 - a custom set, where I replaced both stairs with two turning stairs (one in each way).

As in my set, the stairs are different and not swapable, it took twice as much time (and give twice as much puzzle) than the normal one.

![1](assets/tt-aws1.png)

As you can see above, computing a single graph could take up to 2Go of RAM ! (I haven't seen more than 2.3Go). I use a lot of complete Game instances in a graph. It should be possible to limit that by having the graph state share the blocks set memory...

A simple data format can be found in:
 - [tt-normal.txt](tt-normal.txt)
 - [tt-colimacon.txt](tt-colimacon.txt)

Each line of those files splits as follow:

 - 9 blocks (to be placed in reading order)
 - pawn x position
 - pawn y position
 - number of move to solved state
 - number of state in the full graph of possibles movements
 - the number of solved state in the previous graph (=graph size)
 - the set of the blocks the pawn step over during the solve

Note: The space scanning trick heavily depend of the [14-15 puzzle](https://en.wikipedia.org/wiki/15_puzzle): when considering the resulting possibilities graph, there are only two way to arrange a set of blocks (even only one if two of them are identical).

## Step 2: Filtering the database
There are a lot of puzzles found, but a lot of them are similar. A few things are done to try to prune them into a smaller set, with the following asumption:

 - If puzzles have the same oriented blocks set, and same number of move to solve, they only have a minor blocks place permutation.
 - If puzzles have the same number of move and the same 'stepped over' blocks set, the 'unstepped over' block could be swaped or turned without changing the problem.
 - If puzzles have the same number of move and a graph of the same size, the constraints on the pawn movement are likely similar.
 - In the end just to prune a bit more the leftover puzzles I only keep one for each different graph size.

Here are just a few random puzzle for each blockset :

![normal](assets/normal.gif)
![colimacon](assets/colimacon.gif)


Note: I have implemented the 'stepped over' in a simplistic way that might prune more puzzles than it should, but I'm left with enough puzzle (532 for normal game, 836 for colimacon game), and I prefer to prune more than too little.

## Step 3: Cards
The last script pick puzzles from the groups that was discussed before and turned them into cards.

![card](assets/48v.png)

As I am happy with my previous card game produced, I used [printerstudio](https://www.printerstudio.com/unique-ideas/blank-playing-cards.html) again to print 54 cards deck. I like the 54 cards size has it allow to have 100 puzzle on 50 card, and let 4 extra for various things (header/credit, score card).

You can generate your own deck by tweaking the filtering (to get more difficulty spread for instance) or just use the same as me if you want:

 - The [100 longuest normal puzzle](assets/printed_deck_normal.tar). Probably the one you are looking for.
 - The [100 longuest colimacon puzzle](assets/printed_deck_colimacon.tar). This one will need you to DIY the two additional block (I still need to make mine)...

Note: generating twice the same deck will result in different output, as only one puzzle is chosen at random for each similar puzzles.


TODO: add picture when the deck is received

## Note

### Failed attempt

After a firt attempt, I redesign most of the search space scanning as I had failed to do it properly. I used a simplistic way to ensure that every state is visited and in the end:

 - the same graph was potentially visited several time (taking soooo much time)
 - all of them was not even guaranted to be seen. (easy to spot, I didn't had an equal or more complex puzzle than the harder of the booklet)

I now am much more confident that I completely 'solved' this game. The new method is much more foolproof with the added benefit of being much more logicaly and thus ensure to not recompute twice the same graph.

### optional improvement
I don't give much importance to have a solution available when I print my deck. But every card has an #id at the botom to easily reconstruct the game in a simple format. As I already have a solver, if needed, I'll add this format to the possible input.
