from random import choice
from PIL import Image, ImageFont, ImageDraw
import os
import qrcode
from math import ceil
from collections import defaultdict, namedtuple
import string
import multiprocessing

from game import Block, Game, solve
from cards import *


"""
Slightly modified version of get_puzzles (much more agressive pruning) and gen_deck (output a txt files in addition to the image) to generate spread puzzle of all difficulties. This give ~100 puzzles to give to smartgames so they can made an additional free to download booklet available on their website.
"""


def get_puzzles(db_file):
    try:
        with open(f"selected.txt", "r") as fp:
            print("reusing existing selected.txt file")
            lines = fp.readlines()
        puzzles = [parse(line) for line in lines]
        return puzzles
    except FileNotFoundError:
        # Generate this file if not present
        pass

    try:
        with open(f"tt-{db_file}.txt", "r") as fp:
            lines = fp.readlines()
    except FileNotFoundError:
        raise FileNotFoundError(
            "run `python3 gen_problem.py` (very time comsuming) and `cat data/* > tt.txt` first"
        )

    print(f"{len(lines)} puzzles in txt file")

    puzzles = [parse(line) for line in lines]
    puzzles = [p for p in puzzles if p.dist > 10]  # Way too easy ones

    # same dist
    s = defaultdict(list)
    for p in puzzles:
        s[p.dist].append(p)
    keep = []
    for k in s.keys():
        keep.append(choice(s[k]))
    print(f"{len(keep)} puzzle kept after (dist) pruning")

    # same over
    s = defaultdict(list)
    for p in keep:
        b = "".join(sorted(p.over))
        s[b].append(p)
    keep = []
    for k in s.keys():
        keep.append(choice(s[k]))
    print(f"{len(keep)} puzzle kept after (over) pruning")

    keep.sort(key=lambda p: p.dist)

    with open("selected.txt", "w") as fp:
        for p in keep:
            fp.write(",".join(b for b in p.blocks))
            fp.write(f",{p.px},{p.py},{p.dist},{p.nb_state},{p.nb_win},")
            over = p.over
            fp.write("{")
            fp.write(", ".join(b for b in p.over))
            fp.write("}\n")

    return keep


def treat_single(t):
    i, puzzle = t
    places = {}
    for y in range(3):
        for x in range(3):
            places[x, y] = Block(puzzle.blocks[x + 3 * y])
    game = Game(places, (puzzle.px, puzzle.py))
    G, win, steps = solve(game)
    line = f"{repr(steps[-1])},0,0,0,{{}}"
    end_state = parse(line)

    solution = []
    cur = steps.pop(0)
    for new in steps:
        solution.append(new - cur)
        cur = new
    solution.append("L")

    card = mk_info_card(puzzle, i, end_state, solution, debug=False)
    card.save(os.path.join("selected", f"{i:03}.png"), "PNG")
    print(i)


def gen_deck(db_file, offset=0, deck_id=None):
    print(f"generating {db_file} deck")
    try:
        os.makedirs(os.path.join("selected"))
    except FileExistsError:
        pass

    keep = get_puzzles(db_file)

    pool = multiprocessing.Pool(3)
    jobs = pool.map(treat_single, enumerate(keep, start=1))
    pool.close()
    pool.join()

    if jobs.successful():
        print("done without error")
    else:
        print("an error happend during the jobs")


if __name__ == "__main__":
    gen_deck("normal")
