from game import Block, Game, rotate, red


class FollowPathGame(Game):
    # allow to follow a booklet solution
    def move_pawn(self, way):
        dx = dy = 0
        for c in way:
            cx, cy = {"D": (0, 1), "L": (-1, 0), "U": (0, -1), "R": (1, 0)}[c]
            dx += cx
            dy += cy
        px, py = self.pawn
        new = (px + dx, py + dy)
        move = [move for move in self._moves_pawn() if move.pawn == new]
        return move[0]

    def move_block(self, way):
        px, py = self.no_block
        delta = {"d": (0, -1), "l": (1, 0), "u": (0, 1), "r": (-1, 0)}[way]
        return self._move_block(delta)

    def move(self, way):
        if len(way) > 1 or way.isupper():
            return self.move_pawn(way)
        return self.move_block(way)


def test_rotate():
    assert rotate(1) == 2
    assert rotate(2) == 4
    assert rotate(4) == 8
    assert rotate(8) == 1
    assert rotate(10) == 5
    assert rotate(0x84) == 0x18


def test_block():
    assert Block("u").sides == 128
    assert Block("r").sides == 64
    assert Block("d").sides == 32
    assert Block("l").sides == 16
    assert Block("U").sides == 8
    assert Block("R").sides == 4
    assert Block("D").sides == 2
    assert Block("L").sides == 1

    assert {str(r) for r in Block("Lr").rotations()} == {"▲", "►", "▼", "◄"}
    assert {str(r) for r in Block("ur").rotations()} == {"┌", "┐", "└", "┘"}
    assert {str(r) for r in Block("UD").rotations()} == {"═", "║"}
    assert {str(r) for r in Block("UR").rotations()} == {"╔", "╗", "╚", "╝"}
    assert {str(r) for r in Block("Ur").rotations()} == {"╒", "╖", "╛", "╙"}
    assert {str(r) for r in Block("Ul").rotations()} == {"╓", "╕", "╜", "╘"}


def test_game():
    game = Game(
        {
            (0, 0): Block("RL"),
            (1, 0): Block("Lr"),
            (2, 0): Block("ld"),
            (0, 1): Block("DR"),
            (1, 1): Block(""),
            (2, 1): Block("lu"),
            (0, 2): Block("UR"),
            (1, 2): Block("Lr"),
            (2, 2): Block("ld"),
        },
        (2, 2),
    )
    assert str(game) == f"═◄┐\n╔X┘\n╚◄{red('┐')}\n"
    mb = [str(state) for state in game._moves_block()]
    assert f"═X┐\n╔◄┘\n╚◄{red('┐')}\n" in mb
    assert f"═◄┐\n╔┘X\n╚◄{red('┐')}\n" in mb
    assert f"═◄┐\n╔◄┘\n╚X{red('┐')}\n" in mb
    assert f"═◄┐\nX╔┘\n╚◄{red('┐')}\n" in mb

    assert set(game.walkable()) == {(1, 2)}

    mp = [str(state) for state in game._moves_pawn()]
    assert f"═◄┐\n╔X┘\n╚{red('◄')}┐\n" in mp

    assert Block("Lr") in game.places.values()
    assert game.is_possible()

    alt = Game.from_str("RL,Lr,ld,DR,X,lu,UR,Lr,ld,2,2")
    assert str(alt) == str(game)
    assert repr(alt) == "LR,rL,ld,DR,X,lu,UR,rL,ld,2,2"


def test_solution():
    game = FollowPathGame(
        {
            (0, 0): Block("ld"),
            (1, 0): Block("DR"),
            (2, 0): Block(""),
            (0, 1): Block("ld"),
            (1, 1): Block("LU"),
            (2, 1): Block("Lr"),
            (0, 2): Block("lR"),
            (1, 2): Block("UD"),
            (2, 2): Block("lu"),
        },
        (2, 1),
    )
    print(game)

    it = (
        "rruuldrLull"
        "ddrruullddr"
        "ruullddrruu"
        "LlldruRrddl"
        "luurrddlluu"
        #
        "rrDdlldrurU"
        "ullddrruull"
        "ddrruuLlldd"
        "ruuRrddlluu"
        "rrddlluurrd"
        #
        "dlluuRrdrul"
        "ddLDLrullurd"
        "ldruuldrrRUR"
        "ldruuldrLul"
        "lddrruulldd"
        #
        "rruullddrru"
        "uLlddrRUuLL"
    )
    for c in it:
        print(c)
        game = game.move(c)
        print(game)
    # ended to follow the given path without other error


if __name__ == "__main__":
    test_game()
